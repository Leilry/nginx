#### Running Nginx Docker Container:

```bash
$ docker run --name=nginx --restart=always -p 80:80 -d registry.gitlab.com/leilry/nginx:latest 
```

####  Watch Logs:

```bash
$ docker logs -f nginx
```
####  For finding where stored json log file:
```bash
$ docker inspect nginx | grep Log
```

#### Monitoring
Nginx has logs like:
```sh
10.0.2.15 - - [30/Aug/2020:16:20:49 +0000] "GET /monitoring HTTP/1.1" 200 97 "-" "curl/7.29.0" "-"
```
Whatch for Nginx HTTP Code  You will see of "200" responses. When you begin to see "400" or "500" and above responses, you will know something is wrong.

http://host/monitoring will show you stub status
Example:
```sh
Active connections: 1
server accepts handled requests
 5 5 5
Reading: 0 Writing: 1 Waiting: 0
```
You can also use for better monitoring:
- [Enable Health Checks for your app](https://docs.nginx.com/nginx/admin-guide/load-balancer/http-health-check/)
- [Use ELK stack for nginx logs]( https://medium.com/faun/nginx-logs-monitoring-using-elk-stack-docker-4b64f46e989 )
- [Use NGINX Exporter for sending metrics to Prometheus](https://github.com/nginxinc/nginx-prometheus-exporter#:~:text=NGINX%20Plus%20provides%20a%20richer,to%20be%20collected%20by%20Prometheus.)

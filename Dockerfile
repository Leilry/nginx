FROM nginx:1.12-alpine
WORKDIR /usr/src/app
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
